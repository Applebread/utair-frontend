export const environment = {
    production: false,
    apiEndpoint: "http://localhost:8083",
    imagesBasePath: "http://localhost:8083",
    simbriefApiUrl: "https://api.simbrief.com",
    simbriefToken: "SIMBRIEF_API_TOKEN",
    mapboxApiKey: "pk.eyJ1IjoiYXBwbGVicmVkIiwiYSI6ImNtNmMxYmQydTBlNW4ybHNjazRwZXVqcjQifQ.qht8mQeCCT9crMrfspyn_A"
};
