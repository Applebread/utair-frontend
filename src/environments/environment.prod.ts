export const environment = {
    production: true,
    apiEndpoint: "http://uta-api.borning-sun.info",
    imagesBasePath: "http://uta-api.borning-sun.info",
    simbriefApiUrl: "https://api.simbrief.com",
    simbriefToken: "SIMBRIEF_API_TOKEN",
    mapboxApiKey: "MAPBOX_API_KEY"
};
