import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PublicMainComponent} from "./views/public/main/public-main.component";
import {PublicAboutComponent} from "./views/public/about/public-about.component";
import {FleetComponent} from "./views/public/fleet/fleet.component";
import {LiveMapComponent} from "./views/public/live-map/live-map.component";
import {RegisterComponent} from "./views/public/register/register.component";
import {DownloadComponent} from "./views/public/download/download.component";
import {AppGuardGuard} from "./app-guard.guard";
import {LoginComponent} from "./views/public/login/login.component";
import {DashboardComponent} from "./views/dashboard/dashboard/dashboard.component";
import {ExamsComponent} from "./views/dashboard/exams/exams.component";
import {TripRequestComponent} from "./views/dashboard/trip-request/trip-request.component";
import {ProcessExamComponent} from "./views/dashboard/exams/process-exam/process-exam.component";
import {AppInstructorGuard} from "./app-instructor-guard.guard";
import {InstructorDashboardComponent} from "./views/dashboard/instructor/trainings-list/instructor-dashboard.component";
import {TripBookingComponent} from "./views/dashboard/trip-booking/trip-booking.component";

const routes: Routes = [
  { path: '', component: PublicMainComponent },
  { path: 'about', component: PublicAboutComponent },
  { path: 'fleet', component: FleetComponent },
  { path: 'map', component: LiveMapComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'download', component: DownloadComponent  },
  { path: 'login', component: LoginComponent  },
  {
    path: 'dashboard',
    canActivate: [AppGuardGuard],
    children: [
      { path: 'main', component: DashboardComponent },
      {
        path: 'exams',
        component: ExamsComponent
      },
      { path: 'exam/:id', component: ProcessExamComponent },
      { path: 'trip-request', component: TripRequestComponent },
      { path: 'active-trip', component: TripBookingComponent },
      { path: 'instructor', canActivate: [AppInstructorGuard], children: [
          { path: 'trainings', component: InstructorDashboardComponent }
        ]
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
