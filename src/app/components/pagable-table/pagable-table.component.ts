import {Component, EventEmitter, Input, Output, TemplateRef} from '@angular/core';

export interface IPaginationEvent {
  page: number;
  pageSize: number;
}

export interface IColumn {
  key: string;
  title: string;
}

@Component({
  selector: 'app-pagable-table',
  templateUrl: './pagable-table.component.html',
  styleUrls: ['./pagable-table.component.scss']
})
export class PagableTableComponent {
  @Input() headerTemplate: TemplateRef<any> | null = null;
  @Input() rowTemplate: TemplateRef<any> | null = null;
  @Input() cellTemplate: TemplateRef<any> | null = null;
  @Input() actionsTemplate: TemplateRef<any> | null = null;
  @Input() dataSource: Array<{[key: string]: any}> = [];
  @Input() columns: Array<IColumn> = [];
  @Input() collectionSize: number = 0;
  @Input() page: number = 1;
  @Input() pageSize: number = 15;
  @Output() updatePagination = new EventEmitter<IPaginationEvent>();

  public emitUpdate(page: number, pageSize: number): void {
    this.updatePagination.emit({ page, pageSize })
  }
}
