import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
  ViewChild
} from '@angular/core';

export interface IAutocompleteItem {
  label: string;
  value: string | number;
}

@Component({
  selector: 'app-autocomplete[name][entries]',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements AfterViewInit {
  @Input() public entries: Array<IAutocompleteItem> = [];

  @Input() public label?: string

  @Input() public name?: string;

  @Input() public placeholder?: string;

  public selectedValue?: IAutocompleteItem;

  public opened: boolean = false;

  public filterInputValue: string = '';

  @Input() public value?: IAutocompleteItem;

  @Output() valueChanged = new EventEmitter<IAutocompleteItem>();

  @Output() nativeInput = new EventEmitter<string>();

  @ViewChild('input') input?: ElementRef<HTMLInputElement>;

  @HostListener('document:click', ['$event'])
  documentClick() {
    this.opened = false;
  }

  public filteredEntries(): Array<IAutocompleteItem> {
    return this.entries.filter((item: IAutocompleteItem) => {
      return item.label.toLowerCase().indexOf(this.filterInputValue.toLowerCase()) !== -1;
    })
  }

  public selectEntry(item: IAutocompleteItem): void {
    this.selectedValue = item;
    this.valueChanged.emit(item);
    this.opened = false;
  }

  public filterEntries(event: any): void
  {
    this.filterInputValue = event.target.value;
    this.opened = !!this.filterInputValue.length;
  }

  ngAfterViewInit(): void {
    const el = this.input?.nativeElement;
    // @ts-ignore
    el.value = this.value ? this.value?.label : ''

    this.input?.nativeElement.addEventListener('input', ev => {
        // @ts-ignore
        this.nativeInput.emit(ev.target.value ?? '')
    })
  }
}
