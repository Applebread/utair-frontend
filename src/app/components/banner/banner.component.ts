import {Component, OnInit} from '@angular/core';
import {Banner} from "./value/Banner";
import {BannersCollection} from "./value/BannersCollection";

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  private slidesInterval: ReturnType<typeof setInterval> | null = null;

  public banners: BannersCollection = new BannersCollection(
    new Banner('/assets/banner/banner1.jpg', 'Карта полетов', 'Мы летаем везде', { title: 'Подробнее', link: '/flight-map' }),
    new Banner('/assets/banner/banner2.jpg', 'Реальное расписание', 'Удобные инструменты планирования помогут получить отличный опыт полетов.', { title: 'Подробнее', link: '/schedule' })
  )

  public nextSlide(): void {
    this.banners.next();
    this.runSlides();
  }

  public prevSlide(): void {
    this.banners.prev();
    this.runSlides();
  }

  public runSlides(): void {
    this.resetSlidesInterval();

    this.slidesInterval = setInterval(() => {
      this.banners.next();
    }, 4000)
  }

  public resetSlidesInterval(): void {
    if (this.slidesInterval) {
      clearInterval(this.slidesInterval)
    }
  }

  ngOnInit(): void {
    this.runSlides();
  }
}
