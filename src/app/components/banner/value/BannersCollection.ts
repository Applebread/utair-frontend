import {Banner} from "./Banner";

export class BannersCollection {
  private readonly items: Array<Banner>;

  constructor(...banners: Array<Banner>) {
    this.items = banners;
    this.reset();
  }

  private markAllInactive(): void
  {
    this.items.map(item => item.markInactive());
  }

  prev(): void {
    if (!this.items.length) {
      throw new Error("Impossible to get current of nothing");
    }

    const currentIndex = this.currentIndex();
    this.markAllInactive();
    let prevIndex = typeof this.items[currentIndex - 1] !== "undefined" ? currentIndex - 1 : this.items.length - 1;
    this.items[prevIndex].markActive();
  }

  next(): void {
    if (!this.items.length) {
      throw new Error("Impossible to get current of nothing");
    }

    const currentIndex = this.currentIndex();
    this.markAllInactive();
    let nextIndex = typeof this.items[currentIndex + 1] !== "undefined" ? currentIndex + 1 : 0;
    this.items[nextIndex].markActive();
  }

  current(): Banner {
    const banner = this.items?.find(item => item.isActive())

    if (!banner) {
      throw new Error("Impossible to get current of nothing");
    }

    return banner;
  }

  private currentIndex(): number {
    return this.items.findIndex(item => item.isActive());
  }

  reset(): void {
    this.items.find(() => true)?.markActive();
  }

  public values(): Array<Banner> {
    return this.items;
  }
}
