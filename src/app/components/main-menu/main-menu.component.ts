import { Component } from '@angular/core';
import {MenuItem} from "./value/MenuItem";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent {
  public items: Array<MenuItem> = [
    new MenuItem("О компании", "/about"),
    new MenuItem("Флот", "/fleet"),
    new MenuItem("Карта полетов", "/map"),
    new MenuItem("Зарегистрироваться", "/register"),
    new MenuItem("Загрузки", "/download")
  ];
}
