import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from "./services/auth/auth.service";
import {AppToastService} from "./services/toast/app.toast.service";

@Injectable({
  providedIn: 'root'
})
export class AppInstructorGuard implements CanActivate {
  constructor(private service: AuthService, private router: Router, private toastService: AppToastService) {
  }

  canActivate(
     oute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (!this.service.isLoggedIn() || !this.service.loggedInUser) {
      this.router.navigate(['', 'login']);
      return false;
    }

    if (!this.service.loggedInUser.roles.includes('ROLE_INSTRUCTOR')) {
      this.router.navigate(['', 'login']);
      this.toastService.show('Нет доступа', 'Этот раздел предназначен для инструкторов, пожалуйста войдите как инструктор.');
      return false;
    }

    return true;
  }
}
