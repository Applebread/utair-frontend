import {Inject, Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";
import {Observable} from "rxjs";

export interface ITraining {
    date: string;
    examId: number;
    pilotId: number;
    pilotName: number;
    status: string;
    resultPercent: number | null;
    instructorName: string | null;
    instructorId: number | null;
}

export interface ITrainingList {
    items: Array<ITraining>;
    totalResults: number;
}

@Injectable({
    providedIn: "root"
})
export class InstructorService {
    constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    }

    public waitingTrainingList(page: number = 1, pageSize: number = 5): Observable<ITrainingList> {
        const params = new HttpParams().set('page', page).set('pageSize', pageSize);
        return this.http.get<ITrainingList>(this.config.apiEndpoint + '/api/exam/training/waiting', {params});
    }

    public assignToTraining(examId: number): Observable<void> {
        return this.http.post<void>(this.config.apiEndpoint + '/api/exam/training-assign/' + examId, {});
    }

    public startTraining(examId: number): Observable<void> {
        return this.http.post<void>(this.config.apiEndpoint + '/api/exam/training-started/' + examId, {});
    }

    public stopTraining(examId: number): Observable<void> {
        return this.http.post<void>(this.config.apiEndpoint + '/api/exam/training-stopped/' + examId, {});
    }

    public finishTraining(examId: number, resultPercent: number): Observable<void> {
        return this.http.post<void>(this.config.apiEndpoint + '/api/exam/training-passed/' + examId, {resultPercent: Math.min(100, Math.max(0, resultPercent))});
    }
}
