import {HttpClient, HttpParams} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";
import {Observable} from "rxjs";


export interface ITimetable {
  items: Array<IFlight>;
  totalResults: number;
}

export interface IFlight {
  id: number;
  flightNumber: string;
  from: string;
  to: string;
  aircraftType: string;
  etd: string;
  daysOfWeek: Array<number>;
  scheduledSince: string;
  scheduledTill: string
}

export interface ITimetableRequest {
  aircraftType?: string;
  dateOfFlight?: string;
  from?: string;
  to?: string;
  availableForMe?: boolean;
}

export interface DayOfWeek {
    key: number;
    value: string;
}

@Injectable({providedIn: "root"})
export class TimetableService {
  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
  }

  public getTimetable(request: ITimetableRequest, page: number = 1, pageSize: number = 30): Observable<ITimetable> {
    const params = new HttpParams().set('page', page).set('pageSize', pageSize);
    return this.http.post<ITimetable>(this.config.apiEndpoint + '/api/flight/timetable', request, {params});
  }

  public getFlightDetails(id: number): Observable<IFlight> {
    return this.http.get<IFlight>(this.config.apiEndpoint + '/api/flight/' + id);
  }

  public getAvailableTrips(request: ITimetableRequest, page: number = 1, pageSize: number = 30): Observable<ITimetable> {
    const params = new HttpParams().set('page', page).set('pageSize', pageSize);
    return this.http.post<ITimetable>(this.config.apiEndpoint + '/api/trip/list/available', request, {params});
  }

  public getScheduledAirportsList(): Observable<Array<string>> {
    return this.http.get<Array<string>>(this.config.apiEndpoint + '/api/flight/timetable/airports');
  }

  public getScheduledAircraftTypes(): Observable<Array<string>> {
    return this.http.get<Array<string>>(this.config.apiEndpoint + '/api/flight/timetable/aircraft-types');
  }
}
