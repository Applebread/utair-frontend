import {Inject, Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";

export interface IAirportsList {
    totalResults: number;
    items: Array<IAirport>;
}

export interface IAirport {
    id?: number;
    name: string;
    icao: string;
    latitude: string;
    longitude: string;
    city: string;
    timezone: string;
    source: string;
    currentMetar?: string;
}

@Injectable({
    providedIn: 'root'
})
export class AirportsService {
    constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    }

    public airportList(page: number = 1, pageSize: number = 15): Observable<IAirportsList>
    {
        const params = new HttpParams().set('page', page).set('page_size', pageSize);

        return this.http.get<IAirportsList>(this.config.apiEndpoint + "/api/airports/list", {params, withCredentials: true});
    }

    public alternatesList(icao: string): Observable<Array<IAirport>> {
        return this.http.get<Array<IAirport>>(this.config.apiEndpoint + `/api/airports/${icao}/alternates`)
    }
}
