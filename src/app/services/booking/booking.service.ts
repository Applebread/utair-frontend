import {HttpClient} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";
import {Observable} from "rxjs";
import {IAircraft} from "../aircraft/aircraft.service";
import {IAirport} from "../airport/airports.service";
import {IFlight} from "../timetable/timetable.service";

export interface ITrip {
    id: number;
    captain: string;
    firstOfficer?: string;
    aircraft: IAircraft;
    dutyTime: number;
    from: string
    to: string
    status: string
    legs: Array<ITripLeg>
}

export interface ITripLeg {
    id: number;
    callsign: string;
    averageDistance: number;
    averageDutyTime: number;
    departureTime: string | null;
    selectedAlternates: Array<string>;
    status: string;
    from: IAirport;
    to: IAirport;
}

export interface IUpdateAlternatesRequestInterface {
    alternates: Array<string>
}

@Injectable({providedIn: "root"})
export class BookingService {
    constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {}

    public doBooking(flightId: number, aircraftType: string, etd: string): Observable<ITrip> {
        return this.http.post<ITrip>(this.config.apiEndpoint + `/api/trip/book/${flightId}`, { aircraftType, etd })
    }

    public activeTripInfo(): Observable<ITrip> {
        return this.http.get<ITrip>(this.config.apiEndpoint + `/api/trip-info`);
    }

    public addNewLeg(trip: ITrip, flight: IFlight): Observable<ITrip> {
        return this.http.post<ITrip>(this.config.apiEndpoint + `/api/trip/${trip.id}/add-leg/${flight.id}`, {})
    }

    public removeLastLeg(trip: ITrip): Observable<ITrip> {
        return this.http.delete<ITrip>(this.config.apiEndpoint + `/api/trip/${trip.id}/remove-last-leg`, {})
    }

    public updateTripLegAlternates(trip: ITrip, leg: ITripLeg, alternates: IUpdateAlternatesRequestInterface): Observable<ITrip> {
        return this.http.post<ITrip>(this.config.apiEndpoint + `/api/trip/${trip.id}/leg/${leg.id}/update-alternates`, alternates)
    }

    public startTrip(trip: ITrip): Observable<ITrip> {
        return this.http.post<ITrip>(this.config.apiEndpoint + `/api/trip/${trip.id}/start`, {})
    }
}
