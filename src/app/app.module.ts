import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HeaderComponent} from './layout/public/header/header.component';
import {FooterComponent} from './layout/public/footer/footer.component';
import {MainLayoutComponent} from './layout/public/main-layout/main-layout.component';
import {PublicMainComponent} from './views/public/main/public-main.component';
import {MainMenuComponent} from './components/main-menu/main-menu.component';
import {BannerComponent} from './components/banner/banner.component';
import {PublicAboutComponent} from './views/public/about/public-about.component';
import {FleetComponent} from './views/public/fleet/fleet.component';
import {LiveMapComponent} from './views/public/live-map/live-map.component';
import {RegisterComponent} from './views/public/register/register.component';
import {DownloadComponent} from './views/public/download/download.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AppConfigModule} from "./app-config/app-config.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {LoginComponent} from "./views/public/login/login.component";
import { DashboardHeaderComponent } from './layout/dashboard/dashboard-header/dashboard-header.component';
import { DashboardFooterComponent } from './layout/dashboard/dashboard-footer/dashboard-footer.component';
import { DashboardMainComponent } from './layout/dashboard/dashboard-main/dashboard-main.component';
import { DashboardComponent } from './views/dashboard/dashboard/dashboard.component';
import { DashboardSidebarComponent } from './layout/dashboard/dashboard-sidebar/dashboard-sidebar.component';
import { TripRequestComponent } from './views/dashboard/trip-request/trip-request.component';
import { ExamsComponent } from './views/dashboard/exams/exams.component';
import {AuthTokenInterceptor} from "./interceptors/authToken.Interceptor";
import {SimbriefAuthTokenInterceptor} from "./interceptors/simbriefAuthToken.Interceptor";
import { DashboardUserComponent } from './views/dashboard/dashboard/dashboard-user/dashboard-user.component';
import { DashboardSmallBlockComponent } from './views/dashboard/dashboard/dashboard-small-block/dashboard-small-block.component';
import { ProcessExamComponent } from './views/dashboard/exams/process-exam/process-exam.component';
import { ExamResultComponent } from './views/dashboard/exams/process-exam/exam-result/exam-result.component';
import { TypeRatingBlockComponent } from './views/dashboard/dashboard/type-rating-block/type-rating-block.component';
import { AppToastsComponent } from './components/app-toast/app-toasts.component';
import { InstructorDashboardComponent } from './views/dashboard/instructor/trainings-list/instructor-dashboard.component';
import { PagableTableComponent } from './components/pagable-table/pagable-table.component';
import { TrainingListComponent } from './views/dashboard/instructor/trainings-list/training-list/training-list.component';
import { TimetableComponent } from './views/dashboard/trip-request/timetable/timetable.component';
import { TimetableFilterComponent } from './views/dashboard/trip-request/timetable/timetable-filter/timetable-filter.component';
import { TableColumnDirective } from './components/pagable-table/table-column.directive';
import { AutocompleteComponent } from './components/autocomplete/autocomplete.component';
import { TripBookingComponent } from './views/dashboard/trip-booking/trip-booking.component';
import { NgbDropdown } from "@ng-bootstrap/ng-bootstrap";
import { InputMaskModule } from '@ngneat/input-mask';
import { AircraftStatsModalComponent } from './views/dashboard/trip-booking/aircraft-stats-modal/aircraft-stats-modal.component';
import { LegsManagementComponent } from './views/dashboard/trip-booking/legs-management/legs-management.component';
import { TripBookingMapComponent } from './views/dashboard/trip-booking/trip-booking-map/trip-booking-map.component';
import { TripLegComponent } from './views/dashboard/trip-booking/legs-management/trip-leg/trip-leg.component';
import { TripLegSelectDestinationModalComponent } from './views/dashboard/trip-booking/legs-management/trip-leg/trip-leg-select-destination-modal/trip-leg-select-destination-modal.component';
import { FlightInformationComponent } from './views/dashboard/trip-booking/flight-information/flight-information.component';
import { AircraftInformationComponent } from './views/dashboard/trip-booking/aircraft-information/aircraft-information.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainLayoutComponent,
    PublicMainComponent,
    MainMenuComponent,
    BannerComponent,
    PublicAboutComponent,
    FleetComponent,
    LiveMapComponent,
    RegisterComponent,
    DownloadComponent,
    LoginComponent,
    DashboardHeaderComponent,
    DashboardFooterComponent,
    DashboardMainComponent,
    DashboardComponent,
    DashboardSidebarComponent,
    TripRequestComponent,
    ExamsComponent,
    DashboardUserComponent,
    DashboardSmallBlockComponent,
    ProcessExamComponent,
    ExamResultComponent,
    TypeRatingBlockComponent,
    AppToastsComponent,
    InstructorDashboardComponent,
    PagableTableComponent,
    TrainingListComponent,
    TimetableComponent,
    TimetableFilterComponent,
    TableColumnDirective,
    AutocompleteComponent,
    TripBookingComponent,
    AircraftStatsModalComponent,
    LegsManagementComponent,
    TripBookingMapComponent,
    TripLegComponent,
    TripLegSelectDestinationModalComponent,
    FlightInformationComponent,
    AircraftInformationComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        HttpClientModule,
        AppConfigModule,
        ReactiveFormsModule,
        FormsModule,
        NgbDropdown,
        InputMaskModule
    ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthTokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SimbriefAuthTokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
