import {Component} from '@angular/core';
import {DashboardMenuService, IMenuItem} from "../../../services/dashboard-menu/dashboard-menu.service";

@Component({
  selector: 'app-dashboard-sidebar',
  templateUrl: './dashboard-sidebar.component.html',
  styleUrls: ['./dashboard-sidebar.component.scss']
})
export class DashboardSidebarComponent {
  constructor(private menuService: DashboardMenuService) {
  }

  public get menuItems(): Array<IMenuItem> {
    return this.menuService.availableMenuItems();
  }
}
