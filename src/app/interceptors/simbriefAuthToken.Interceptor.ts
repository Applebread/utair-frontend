import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {catchError, Observable, throwError} from "rxjs";
import {APP_CONFIG, AppConfig} from "../app-config/app-config.module";

@Injectable()
export class SimbriefAuthTokenInterceptor implements HttpInterceptor {
    constructor(@Inject(APP_CONFIG) private config: AppConfig) {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const simbriefToken = this.config.simbriefToken;

        if (request.url.includes(this.config.simbriefApiUrl) && simbriefToken) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${simbriefToken}`,
                },
            });
        }

        return next.handle(request).pipe(
            catchError((err) => {
                return throwError(err);
            })
        );
    }
}
