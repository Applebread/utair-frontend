import { NgModule, InjectionToken } from '@angular/core';
import { environment } from '../../environments/environment';

export let APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export class AppConfig {
    apiEndpoint: string = '';
    simbriefToken: string = '';
    imagesBasePath: string = '';
    simbriefApiUrl: string = '';
    mapboxApiKey: string = '';
}

export const APP_DI_CONFIG: AppConfig = {
    apiEndpoint: environment.apiEndpoint,
    simbriefToken: environment.simbriefToken,
    imagesBasePath: environment.imagesBasePath,
    simbriefApiUrl: environment.simbriefApiUrl,
    mapboxApiKey: environment.mapboxApiKey
};

@NgModule({
    providers: [{
        provide: APP_CONFIG,
        useValue: APP_DI_CONFIG
    }]
})
export class AppConfigModule { }
