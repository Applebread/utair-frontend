import {Inject, Injectable} from "@angular/core";
import { APP_CONFIG, AppConfig } from '../../app-config.module';

@Injectable({
    providedIn: 'root'
})
export class ConfigService {
    constructor(@Inject(APP_CONFIG) private config: AppConfig) {}

    public getApiEndpoint(): string
    {
        return this.config.apiEndpoint;
    }

    public getImagesBasePath(): string
    {
        return this.config.imagesBasePath;
    }

}
