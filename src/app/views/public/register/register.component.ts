import { Component } from '@angular/core';
import {
  IRegistrationData,
  RegistrationErrorCollection,
  RegistrationService
} from "../../../services/registration/registration.service";
import {FormBuilder, FormControl} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../../../services/auth/auth.service";
import {Obj} from "@popperjs/core";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  public registerForm = this.formBuilder.group({
    login: '',
    email: '',
    fullName: '',
    birthDate: '',
    password: '',
    confirmPassword: ''
  });

  public confirmRulesControl = new FormControl();

  public inProgress: boolean = false;

  public errors?: RegistrationErrorCollection | null = null

  constructor(private service: RegistrationService, private formBuilder: FormBuilder, private router: Router, private authService: AuthService) {
    this.registerForm.statusChanges.subscribe(() => {
      this.errors = null;
    })

    this.confirmRulesControl.setValue(false);
  }

  public getError(key: string): string | null {
    if (!this.errors || !this.errors[key]) {
      return null;
    }

    return this.errors[key];
  }

  public doRegister(): void {
    if (!this.confirmRulesControl.value) {
      return;
    }

    const data: IRegistrationData = <IRegistrationData>this.registerForm.value;

    this.inProgress = true;
    this.service.register(data).subscribe(response => {
      this.authService.login(data.login, data.password, true).subscribe(() => {
        this.router.navigate(['/', 'dashboard', 'main'])
        this.inProgress = false;
      });
    }, (errors: HttpErrorResponse) => {
      this.errors = <RegistrationErrorCollection>errors.error.errors;
      this.inProgress = false;
    })
  }
}
