import {Component} from '@angular/core';

import {FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import {AuthService} from "../../../services/auth/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public formData: FormGroup = this.formBuilder.group({
    userName: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  public error: string | null = null

  public inProgress: boolean = false

  constructor(public authService: AuthService, private formBuilder: FormBuilder) {
    this.formData.statusChanges.subscribe(() => {
      this.error = null;
    })
  }

  get userName() {
    return this.formData.get('userName');
  }

  get password() {
    return this.formData.get('password');
  }

  async onClickSubmit() {
    if (this.userName?.errors || this.password?.errors) {
      return;
    }

    this.inProgress = true;
    const data = this.formData.value;
    this.authService.login(data.userName, data.password).subscribe(() => {
      this.error = null;
      this.inProgress = false;
    }, err => {
      this.error = err.error.message;
      this.inProgress = false;
    });
  }
}
