import {Component, OnInit} from '@angular/core';
import {ExamService, IExam, IExamCollection, ExamType} from "../../../services/exam/exam.service";

@Component({
  selector: 'app-exams',
  templateUrl: './exams.component.html',
  styleUrls: ['./exams.component.scss']
})
export class ExamsComponent implements OnInit {
  public exams?: IExamCollection

  constructor(private examService: ExamService) {
  }

  isDateApplicable(exam: IExam): boolean {
    return this.examService.isExamDateApplicable(exam);
  }

  isTraining(exam: IExam): boolean {
    return exam.type === ExamType.Training
  }

  ngOnInit(): void {
    this.examService.getExamList().subscribe(exams => {
      this.exams = exams;
    })
  }
}
