import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ExamService, IExam, IExamResult, IQuestion, IQuestionOption} from "../../../../services/exam/exam.service";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-process-exam',
  templateUrl: './process-exam.component.html',
  styleUrls: ['./process-exam.component.scss']
})
export class ProcessExamComponent implements OnInit{
  public exam?: IExam;

  public result: { [index:number]: Array<number> } = {};

  public progress: boolean = false;

  constructor(private route: ActivatedRoute, private examService: ExamService) {
  }

  updateData(event: any, question: IQuestion, option: IQuestionOption): void {
    if (this.result[question.id] === undefined) {
      this.result[question.id] = [];
    }

    if (question.onlyOneAnswerIsPossible) {
      this.result[question.id] = [option.id]
      this.examService.makeAnswer(question.id, this.result[question.id]).subscribe(() => {});

      return;
    }

    if (event.target.checked) {
      this.result[question.id].push(option.id)
    } else {
      const indexOfChecked = this.result[question.id].findIndex((item: number) => item === parseInt(event.target.value))

      this.result[question.id].splice(indexOfChecked, 1);
    }

    this.examService.makeAnswer(question.id, this.result[question.id]).subscribe(() => {});
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');

    if (!id) {
      throw Error("id is not specified");
    }

    this.examService.getExamDetails(parseInt(id)).subscribe(exam => {
      this.exam = exam
      exam.questions?.forEach((question: IQuestion) => {
        question.options.forEach((option:IQuestionOption) => {
          if (option.checked) {
            if (!this.result[question.id]) {
              this.result[question.id] = [];
            }

            if (question.onlyOneAnswerIsPossible) {
              this.result[question.id] = [option.id]
              return;
            }

            this.result[question.id].push(option.id)
          }
        })
      })
    });
  }

  isAllAnswersDone(): boolean {
    return <boolean>this.exam?.questions?.every((question: IQuestion) => {
      if (this.result[question.id] === undefined) {
        return false;
      }

      return this.result[question.id].length > 0;
    });
  }

  submitTest(): void {
    if (!this.isAllAnswersDone() || !this.exam || this.progress) {
      return;
    }

    this.progress = true;

    this.examService.checkExam(this.exam.id).subscribe((examResult: IExamResult) => {
      if (!this.exam) {
        return;
      }

      this.exam.examResult = examResult;
      this.progress = false;
    });
  }
}
