import {Component, Input} from '@angular/core';
import {ExamService, IExam} from "../../../../../services/exam/exam.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-exam-result',
  templateUrl: './exam-result.component.html',
  styleUrls: ['./exam-result.component.scss']
})
export class ExamResultComponent {
  @Input() public exam?: IExam;


  constructor(private examService: ExamService, private router: Router) {
  }

  reassignFailedExam(): void {
    if (!this.exam) {
      throw new Error("No exam specified");
    }

    if (!this.exam.examResult) {
      throw new Error("Impossible to reassign non-passed exam");
    }

    if (this.exam.examResult.isSuccess) {
      throw new Error("Impossible to reassign successfully passed exam");
    }

    this.examService.reassignFailedExam(this.exam.id).subscribe(() => {
      this.router.navigate(['/', 'dashboard', 'exams'])
    })
  }

  successMessage(exam: IExam): string {
    if (exam.type === 'common') {
      return 'Поздравляем! общий экзамен был успешно пройден.'
    } else if (exam.type === 'typeRating') {
      return 'Поздравляем! Допуск был успешно получен.'
    } else if (exam.type === 'minimum') {
      return 'Поздравляем! Метеоминимум был улучшен.'
    } else if (exam.type === 'training') {
      return 'Поздравляем! Тренировка была инициирована. Свяжитесь со своем инструктором'
    }

    return '';
  }
}
