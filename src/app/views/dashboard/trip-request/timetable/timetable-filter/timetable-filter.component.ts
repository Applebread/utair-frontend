import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import {IAutocompleteItem} from "../../../../../components/autocomplete/autocomplete.component";
import {IFlight, TimetableService} from "../../../../../services/timetable/timetable.service";
import {NgbDate} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute, Route, Router} from "@angular/router";

export interface ITimetableFilterValues {
    from?: string;
    to?: string;
    dateOfFlight?: string;
    aircraftType?: string;
    availableForMe?: boolean
}

@Component({
    selector: 'app-timetable-filter',
    templateUrl: './timetable-filter.component.html',
    styleUrls: ['./timetable-filter.component.scss']
})
export class TimetableFilterComponent implements OnChanges, OnInit {
    public filterValues: ITimetableFilterValues = {
        from: '',
        to: '',
        dateOfFlight: this.todayString(),
        aircraftType: '',
        availableForMe: false
    };

    @Input() public flights: Array<IFlight> = [];

    public allAirports: Array<IAutocompleteItem> = [];

    public allAircraftTypes: Array<IAutocompleteItem> = [];

    @Output() public filterUpdated = new EventEmitter<ITimetableFilterValues>();

    @ViewChild('fromInput')
    public fromInput: any

    @ViewChild('toInput')
    public toInput: any

    constructor(private readonly timetableService: TimetableService, private readonly route: ActivatedRoute, private readonly router: Router) {
    }

    ngOnInit(): void {
        this.filterValues.from = this.route.snapshot.queryParams['from'];
        this.filterValues.to = this.route.snapshot.queryParams['to'];
        this.filterValues.dateOfFlight = this.route.snapshot.queryParams['dateOfFlight'] ?? this.todayString();
        this.filterValues.aircraftType = this.route.snapshot.queryParams['aircraftType'];
        this.filterValues.availableForMe = this.route.snapshot.queryParams['availableForMe'] === 'true';
        this.filterUpdated.emit(this.filterValues);
    }

    private updateAllAirports(): void {
        this.timetableService.getScheduledAirportsList().subscribe(airports => {
            this.allAirports = [];
            airports.forEach(airportIcao => this.allAirports.push({label: airportIcao, value: airportIcao}));
        })
    }

    private updateScheduleAircraftTypes(): void {
        this.timetableService.getScheduledAircraftTypes().subscribe(airports => {
            this.allAircraftTypes = [];
            airports.forEach(aircraftIcao => this.allAircraftTypes.push({label: aircraftIcao, value: aircraftIcao}));
        })
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (typeof changes['flights'] === 'undefined') {
            return;
        }

        this.updateAllAirports();
        this.updateScheduleAircraftTypes();
    }

    public filterSubmit(): void {
        this.filterUpdated.emit(this.filterValues);
        this.updateUrlValues()
    }

    public updateUrlValues(): void {
        this.router.navigate(
            [],
            {
                relativeTo: this.route,
                queryParams: {
                    from: this.filterValues.from,
                    to: this.filterValues.to,
                    dateOfFlight: this.filterValues.dateOfFlight,
                    aircraftType: this.filterValues.aircraftType,
                    availableForMe: this.filterValues.availableForMe
                },
                queryParamsHandling: 'merge'
            }
        )
    }

    public updateScheduleDate(data: NgbDate): void {
        this.filterValues.dateOfFlight = this.createDateStringFromNgb(data);
    }

    private todayString(): string {
        const now = new Date();
        return this.createDateStringFromNgb(new NgbDate(now.getFullYear(), now.getMonth(), now.getDate()));
    }

    private createDateStringFromNgb(data: NgbDate): string {
        const month = data.month === 0 ? 0 : data.month - 1;
        const date = new Date(data.year, month, data.day + 1);
        return date.toISOString().replace(/T.*/gm, '');
    }

    updateAircraftType($event: Event) {
        //@ts-ignore
        this.filterValues.aircraftType = $event.target.value
    }
}
