import {Component, Input, OnInit} from '@angular/core';
import {ITypeRating, LogbookInterface, LogbookService} from "../../../../services/logbook/logbook.service";
import {NgbModal, NgbModalRef,} from "@ng-bootstrap/ng-bootstrap";
import {AircraftService, IAircraftType, IAircraftTypes} from "../../../../services/aircraft/aircraft.service";
import {ExamService, ExamType, IExam, IExamCollection} from "../../../../services/exam/exam.service";
import {FormControl} from "@angular/forms";
import {AppToastService} from "../../../../services/toast/app.toast.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-type-rating-block',
  templateUrl: './type-rating-block.component.html',
  styleUrls: ['./type-rating-block.component.scss']
})
export class TypeRatingBlockComponent implements OnInit {
  @Input() public logbook?: LogbookInterface;

  public typeRatings?: Array<ITypeRating>;
  private modal: NgbModalRef | null = null;
  public typeRatingFormInput = new FormControl();
  public requestedExam?: IExam
  public exams?: IExamCollection

  constructor(
    private modalService: NgbModal,
    private aircraftService: AircraftService,
    private logbookService: LogbookService,
    private examService: ExamService,
    private toasts: AppToastService,
    public router: Router
  ) {
  }

  ngOnInit(): void {
    this.logbookService.getTypeRatingsList().subscribe((ratings: Array<ITypeRating>) => {
      this.typeRatings = ratings;
    });

    this.examService.getExamList().subscribe((exams: IExamCollection) => {
      this.exams = exams;
    });
  }

  availableTypeRatings(): Array<ITypeRating> {
    if (!this.typeRatings) {
      return [];
    }

    return this.typeRatings;
  }

  requestRatingExam(): void {
    if (!this.modal) {
      return;
    }

    const selectedRating = parseInt(this.typeRatingFormInput.value);
    this.examService.requestExam(ExamType.TypeRating, selectedRating).subscribe((exam: IExam) => {
      this.requestedExam = exam;

      if (this.modal) {
        this.modal.close();
        this.toasts.show("Создан новый экзамен", 'Вы можете увидеть его в разделе экзаменов.');
      }
    })
  }

  public getTypeRatingAcTypes(typeRating: ITypeRating): string {
    return typeRating.aircraftTypes.map(type => type.name).join(', ');
  }

  get typeRatingExams(): Array<IExam> {
    if (!this.exams || !this.exams.items) {
      return [];
    }

    return this.exams.items.filter((exam: IExam) => exam.type === ExamType.TypeRating && !exam.isPassed && this.isDateApplicable(exam));
  }

  isDateApplicable(exam: IExam): boolean {
    return this.examService.isExamDateApplicable(exam);
  }

  openAddRatingModal(content: any): void {
    if (this.typeRatingExams.length) {
      return;
    }

    this.modal = this.modalService.open(content, {ariaLabelledBy: 'add-new-type-rating'});
  }
}
