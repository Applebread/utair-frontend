import {Component, Input} from '@angular/core';
import {ITrip} from "../../../../services/booking/booking.service";

@Component({
  selector: 'app-aircraft-stats-modal',
  templateUrl: './aircraft-stats-modal.component.html',
  styleUrls: ['./aircraft-stats-modal.component.scss']
})
export class AircraftStatsModalComponent {
    @Input() tripInfo?: ITrip;
    @Input() modal: any;
}
