import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DayOfWeek, IFlight, ITimetable, TimetableService} from "../../../../../../services/timetable/timetable.service";
import {IColumn, IPaginationEvent} from "../../../../../../components/pagable-table/pagable-table.component";
import {ITrip, ITripLeg} from "../../../../../../services/booking/booking.service";

@Component({
    selector: 'app-trip-leg-select-destination-modal',
    templateUrl: './trip-leg-select-destination-modal.component.html',
    styleUrls: ['./trip-leg-select-destination-modal.component.scss']
})
export class TripLegSelectDestinationModalComponent implements OnInit {
    @Input() previousLeg?: ITripLeg;
    @Input() tripInfo?: ITrip;
    @Input() timeTable: ITimetable = {totalResults: 0, items: []};
    @Output() destinationSelected: EventEmitter<IFlight> = new EventEmitter<IFlight>();
    @Output() modalClosed: EventEmitter<void> = new EventEmitter<void>();
    public tableColumns: Array<IColumn> = [
        {
            key: 'flightNumber',
            title: 'Номер рейса',
        },
        {
            key: 'from',
            title: 'Из',
        },
        {
            key: 'to',
            title: 'В',
        },
        {
            key: 'etd',
            title: 'Время вылета',
        },
        {
            key: 'aircraftType',
            title: 'Тип ВС'
        }
    ];

    public daysOfWeek: Array<DayOfWeek> = [
        {key: 1, value: 'Пн'},
        {key: 2, value: 'Вт'},
        {key: 3, value: 'Ср'},
        {key: 4, value: 'Чт'},
        {key: 5, value: 'Пт'},
        {key: 6, value: 'Сб'},
        {key: 7, value: 'Вс'},
    ];
    public page: number = 1;
    public pageSize: number = 15;

    constructor(private timetableService: TimetableService) {
    }

    ngOnInit(): void {
        this.updateTimetable({
            page: 1,
            pageSize: 15
        });
    }

    updateTimetable(event?: IPaginationEvent): void {
        if (!event || !this.previousLeg || !this.tripInfo) return;

        this.page = event.page;
        this.pageSize = event.pageSize;

        this.timetableService.getAvailableTrips({
            from: this.previousLeg.from.icao,
            availableForMe: false
        }, this.page, this.pageSize).subscribe((timetable: ITimetable) => {
            this.timeTable = timetable;
        })
    }

    selectNewLegDestination(flight: IFlight): void {
        this.destinationSelected.emit(flight);
    }
}
