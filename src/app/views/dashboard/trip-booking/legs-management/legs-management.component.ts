import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BookingService, ITrip, ITripLeg} from "../../../../services/booking/booking.service";

@Component({
  selector: 'app-legs-management',
  templateUrl: './legs-management.component.html',
  styleUrls: ['./legs-management.component.scss']
})
export class LegsManagementComponent {
    @Input()  tripInfo?: ITrip;
    @Output() legAdded: EventEmitter<ITripLeg> = new EventEmitter<ITripLeg>();
    @Output() legRemoved: EventEmitter<void> = new EventEmitter<void>();
    @Output() tripUpdated: EventEmitter<ITrip> = new EventEmitter<ITrip>();

    constructor(private bookingService: BookingService) {
    }

    addLeg(): void {
        if (!this.tripInfo) return;

        const lastLeg = this.tripInfo.legs[this.tripInfo.legs.length - 1];
        const newLeg = {
            id: Date.now(),
            callsign: 'New leg',
            status: 'created',
            from: {...lastLeg.to},
            to: {
                id: 0,
                icao: '',
                name: '',
                latitude: '',
                longitude: '',
                city: '',
                timezone: '',
                source: 'manual',
            },
            averageDutyTime: 0,
            averageDistance: 0,
            departureTime: null,
            selectedAlternates: [],
        };

        this.tripInfo.legs.push(newLeg);
        this.legAdded.emit(newLeg);
    }

    removeLastLeg(): void {
        if (!this.tripInfo || this.tripInfo.legs.length <= 1) return;

        const removedLeg:any = this.tripInfo.legs.pop();
        this.bookingService.removeLastLeg(this.tripInfo).subscribe((trip: ITrip)=> {
            this.legRemoved.emit(removedLeg);
            this.emitTripUpdated(trip);
        })
    }

    emitTripUpdated(trip: ITrip) {
        this.tripUpdated.emit(trip);
    }
}
