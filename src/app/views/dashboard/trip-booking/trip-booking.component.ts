import {Component, OnInit} from '@angular/core';
import {ITrip, ITripLeg} from "../../../services/booking/booking.service";
import {BookingService} from "../../../services/booking/booking.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-trip-booking',
    templateUrl: './trip-booking.component.html',
    styleUrls: ['./trip-booking.component.scss'],
})
export class TripBookingComponent implements OnInit {
    tripInfo: ITrip | undefined;

    constructor(private bookingService: BookingService) {
    }

    ngOnInit(): void {
        this.loadTripInfo();
    }

    loadTripInfo(): void {
        this.bookingService.activeTripInfo().subscribe((data: ITrip) => {
            this.tripInfo = data;
        });
    }

    startTrip(): void {
        if (!this.tripInfo) return;
        this.bookingService.startTrip(this.tripInfo).subscribe((trip:ITrip) => {
            this.tripInfo = trip;
        })
    }
}
