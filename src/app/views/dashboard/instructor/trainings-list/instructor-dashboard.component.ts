import {Component, OnInit} from '@angular/core';
import {InstructorService, ITraining, ITrainingList} from "../../../../services/exam/instructor.service";
import {IPaginationEvent} from "../../../../components/pagable-table/pagable-table.component";

@Component({
  selector: 'app-trainings-list',
  templateUrl: './instructor-dashboard.component.html',
  styleUrls: ['./instructor-dashboard.component.scss']
})
export class InstructorDashboardComponent implements OnInit {
  public trainingList: ITrainingList | null = null;

  constructor(private instructorService: InstructorService) {
  }

  assignToTraining(training: ITraining): void {
    console.log(training)
  }

  updateTrainingList(event: IPaginationEvent): void {
    this.instructorService.waitingTrainingList(event.page, event.pageSize).subscribe((list: ITrainingList) => {
      this.trainingList = list;
    })
  }

  ngOnInit(): void {
    this.updateTrainingList({page: 1, pageSize: 15})
  }
}
