import {Component, OnInit} from '@angular/core';
import {InstructorService, ITraining, ITrainingList} from "../../../../../services/exam/instructor.service";
import {IColumn, IPaginationEvent} from "../../../../../components/pagable-table/pagable-table.component";
import {LogbookInterface, LogbookService} from "../../../../../services/logbook/logbook.service";
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {FormControl, Validators} from "@angular/forms";
import {AppToastService} from "../../../../../services/toast/app.toast.service";

@Component({
    selector: 'app-training-list',
    templateUrl: './training-list.component.html',
    styleUrls: ['./training-list.component.scss']
})
export class TrainingListComponent implements OnInit {
    public trainingList: ITrainingList | null = null;
    public trainingResultPercent: FormControl = new FormControl();
    public currentTrainingToFinish?: ITraining;
    public page: number = 1;
    public pageSize: number = 5;
    public logbook?: LogbookInterface
    private modal: NgbModalRef | null = null;

    constructor(private instructorService: InstructorService, logbookService: LogbookService, private readonly modalService: NgbModal, private readonly toasts: AppToastService) {
        logbookService.getLogbook().subscribe((logbook: LogbookInterface) => {
            this.logbook = logbook;
        })
    }

    get tableColumns(): Array<IColumn> {
        return [
            {
                key: 'rating',
                title: ""
            },
            {
                key: 'date',
                title: "Создан"
            },
            {
                key: 'pilotName',
                title: "Пилот"
            },
            {
                key: 'instructorName',
                title: "Инструктор"
            },
            {
                key: 'status',
                title: "Статус"
            }
        ]
    }

    assignToTraining(training: ITraining): void {
        this.instructorService.assignToTraining(training.examId).subscribe(() => {
            this.updateTrainingList();
        })
    }

    startTraining(training: ITraining): void {
        this.instructorService.startTraining(training.examId).subscribe(() => {
            this.updateTrainingList();
        })
    }

    openFinishTrainingModal(content: any, training: ITraining): void {
        this.modal = this.modalService.open(content, {ariaLabelledBy: 'finish training'});
        this.currentTrainingToFinish = training;
    }

    finishTraining(training?: ITraining): void {
        if (!this.modal || !training) {
            return;
        }

        const resultPercent = parseInt(this.trainingResultPercent.value);

        this.instructorService.finishTraining(training.examId, resultPercent).subscribe(() => {
            if (this.modal) {
                this.modal.close();
                this.toasts.show("Тренажер завершен.", 'Пилот увидит его результаты сразу.');
            }
            this.currentTrainingToFinish = undefined;
            this.trainingResultPercent.setValue(0);

            this.updateTrainingList();
        })
    }

    stopTraining(training: ITraining): void {
        this.instructorService.stopTraining(training.examId).subscribe(() => {
            this.updateTrainingList();
        })
    }

    updateTrainingList(event: IPaginationEvent | null = null): void {
        this.instructorService.waitingTrainingList(event?.page, event?.pageSize).subscribe((list: ITrainingList) => {
            this.trainingList = list;
        })
    }

    ngOnInit(): void {
        this.updateTrainingList({page: 1, pageSize: 15})
        this.trainingResultPercent.addValidators([Validators.min(0), Validators.max(100)]);
    }
}
